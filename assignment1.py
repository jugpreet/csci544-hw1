__author__ = 'jugpreet'

import sys

def main(argv):
    if len(argv) < 2:
        print ("Incorrect number of Arguments")
        sys.exit(0)
    inputFileLoc = argv[0]
    outputFileLoc = argv[1]
    inputFile = open(inputFileLoc, 'r')
    outputFile = open(outputFileLoc, 'w')
    for line in inputFile:
        count = 0
        line = str(line).strip()
        words = str(len(line.split()))
        outputFile.write(words)
        outputFile.write('\n')

if __name__=="__main__" : main(sys.argv[1:])





